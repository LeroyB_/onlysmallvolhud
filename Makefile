# export TARGET = simulator:clang::13.0
# export ARCHS = x86_64

export TARGET = iphone:13.0
export ARCHS = arm64 arm64e
export SYSROOT = /Users/leroybonventre/theos/sdks/iPhoneOS13.0.sdk

export INSTALL_TARGET_PROCESSES = SpringBoard

export DEBUG = 0
export FINALPACKAGE = 1

include $(THEOS)/makefiles/common.mk

TWEAK_NAME = OnlySmallVolHUD

OnlySmallVolHUD_FILES = Tweak.x
OnlySmallVolHUD_CFLAGS = -fobjc-arc

include $(THEOS_MAKE_PATH)/tweak.mk
